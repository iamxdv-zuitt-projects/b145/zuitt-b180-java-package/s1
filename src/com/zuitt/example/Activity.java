package com.zuitt.example;
import java.util.Scanner;

public class Activity {

    public static void main(String[] args) {
        Scanner scannerName = new Scanner(System.in);

        // Activity for S1
        /*
         * 1. Create a new Java project named activity
         * 2. Declare the ff variables to store the user's information
         *   a. fname
         *   b. lname
         *   c. firstSubj
         *   d. secondSubj
         *   e. thirdSubject
         * 3. instantiate/create a new Scanner Object.
         * 4. Print out message asking for the user's input and gather them using the Scanner methods
         * 5. Get the average of the user's input printed out as an integer value.
         * 6. Printout a message that will display the user's full name and the average grade.
         * 7. create a personal Group in Gitlab and name it as "zuitt-b180"
         * 8. Inside the zuitt-b180 group, create a git repository named WDC043-S01
         *   a. Make sure the group and projects are public
         * 9. Initialize a local git repository in your s1 folder, add the remote link and push to got with the
         *   commit message "Add activity code"
         * 10. Link your online repo in boodle.*/

        System.out.println("Firstname: ?");
        String fname = scannerName.nextLine();

        System.out.println("Lastname: ?");
        String lname = scannerName.nextLine();
        System.out.println("1st Subj Grade: ?");
        double firstSubj = scannerName.nextInt();
        System.out.println("2nd Subj Grade: ?");
        double secondSubj = scannerName.nextInt();
        System.out.println("3rd Subj Grade: ?");
        double thirdSubject = scannerName.nextInt();
        System.out.println("Hej " + fname + " " + lname + " " + "Your Average Grade is: " +  (firstSubj + secondSubj + thirdSubject)/3 );

    }
}
